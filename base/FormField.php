<?php

class FormField
{
	public $property;
	public $options;
	public $form;
	
	function __construct($property, $form, $options = [])
	{
		$this->property = $property;
		$this->form = $form;
		$this->options = $options;
	}
	
	function __call($name, $arguments)
	{
		$arguments['options'] = isset($arguments[0]) ? $arguments[0] : [];
		unset($arguments[0]);
		$arguments['options']['class'] = (isset($arguments['options']['class']) ? $arguments['options']['class'] : '').' form-control '.$this->form->layout['field'];
		$arguments['options']['class'] .= $this->form->model->hasError($this->property) ? ' error' : '';
		$arguments['options']['name'] = $this->form->model->shortName().':'.$this->property;
		$arguments['options']['value'] = $name != 'password' ? $this->form->model->{$this->property} : null;
		
		$arguments['options'] = $name == 'textarea' ? $arguments['options'] : Html::stringify($arguments['options']);
		
		if (!method_exists($this, '_'.$name))
		{
			throw new BadMethodCallException;
		}
		
		return call_user_func_array([$this, '_'.$name], $arguments);
	}
	
	function _text($options = "")
	{
		return $this->field('text', $options);
	}
	
	function _password($options = "")
	{
		return $this->field('password', $options);
	}
	
	function _textarea($options = "")
	{
		return $this->wrap(Html::tag('textarea', $options['value'], $options));
	}
	
	function _date($options = "")
	{
		return $this->field('date', $options);
	}
	
	function _time($options = "")
	{
		return $this->field('time', $options);
	}
	
	function _dateTime($options = "")
	{
		return $this->field('datetime-local', $options);
	}
	
	function _radio($options = "")
	{
	}
	
	function _checkbox($options = "")
	{
		
	}
	
	function _dropdown($options = "")
	{
		
	}
	
	private function field($type, $options)
	{
		return $this->wrap("<input type=\"$type\" $options />");
	}
	
	private function wrap($html)
	{
		$labelText = isset($this->form->model->attributeLabels()[$this->property]) ? $this->form->model->attributeLabels()[$this->property] : String::toWords($this->property);
		$label = Html::label($this->form->model->shortName().':'.$this->property, $labelText, ['class' => $this->form->layout['label']]);
		$options = $this->options;
		$options['class'] = (isset($options['class']) ? $options['class'] : '').' form-group';
		$formGroup = Html::tag('div', $label.$html, $options, false);
		return $formGroup;
	}
}
