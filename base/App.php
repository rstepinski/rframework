<?php

class App	
{
	public static $database;
	public static $config;
    public static $controller;
	public static $user = null;
	public static $post = [];
	public static $flash = null;
	public static $referer = null;
	
	static function init()
	{		
		self::$referer = isset($_SESSION['HTTP_REFERER']) ? $_SESSION['HTTP_REFERER'] : Url::to('site/index');
		
		self::$user = isset($_SESSION['user']) ? $_SESSION['user'] : null;
		
		self::$config = require('config/config.php');
		self::$database = new mysqli(self::$config['db']['host'], self::$config['db']['user'], self::$config['db']['pass'], self::$config['db']['db']);
		
		self::$post = $_POST;
	}
	
	static function goBack()
	{
		self::redirect(self::$referer);
	}
	
	static function login($user)
	{
		self::$user = $user;
		$_SESSION['user'] = self::$user;
		self::redirect('site/index');
	}
	
	static function logout()
	{
		self::$user = null;
		$_SESSION['user'] = null;
	}
	
	static function redirect($where)
	{
		header("Location: $where");
	}
	
	static function hasFlash()
	{
		self::$flash = isset($_SESSION['flash']) ? $_SESSION['flash'] : null;
		return (bool) self::$flash;
	}
	
	static function getFlash()
	{
		self::$flash = isset($_SESSION['flash']) ? $_SESSION['flash'] : null;
		
		if (self::hasFlash())
		{
			array_reverse(self::$flash);
			$e = array_pop($self::$flash);
			array_reverse(self::$flash);
			
			$_SESSION['flash'] = self::$flash;
			
			return $e;
		}
		return null;
	}
	
	static function getAllFlashes()
	{
		self::$flash = isset($_SESSION['flash']) ? $_SESSION['flash'] : null;	
		$_SESSION['flash'] = null;
		return self::$flash;
	}
	
	static function addFlash($type, $message)
	{
		self::$flash[] = ['type' => $type, 'message' => $message];
		$_SESSION['flash'] = self::$flash;
	}
	
	static function loggedIn()
	{
		return self::$user !== null;
	}
	
	static function accessLevel()
	{
		if (!App::loggedIn())
		{
			return BaseController::ACCESS_GUEST;
		}		
		
		switch ($user->role)
		{
			case User::ROLE_USER: return BaseController::ACCESS_USER;
			case User::ROLE_ADMIN: return BaseController::ACCESS_ADMIN;
			default: return BaseController::ACCESS_GUEST;
		}
	}
	
	static function ftpUpload()
	{
		$dev = DEBUG_MODE == DEBUG_DEV;
		
		if (!@ftp_connect(self::$config['ftp']['host'], 21))
		{
			if ($dev) { throw new Exception("Nie można było się połączyć z hostem FTP"); }
			return false;
		}
		
		if (!@ftp_login($ftp, self::$config['ftp']['user'], self::$config['ftp']['pass']))
		{
			if ($dev) { throw new Exception("Niepoprawne dane logowania FTP"); }
			return false;
		}
		
		$file = $_FILES['file']['name'];
		$tmpFile = $_FILES['file']['tmp_name'];
		$path = self::$config['ftp']['baseDir'].'/'.self::$config['app']['uploadPath'];
		$filePath = $path.'/'.$file; 
		
		if($_FILES['file']['size'] > self::$config['app']['maxFileSize'])
		{
			return false;
		}
		
		if (!@ftp_put($ftp, $filePath, $tmpFile, FTP_ASCII))
		{
			if ($dev) { throw new Exception("Nie udało się umieścić pliku na serwerze FTP"); }
			return false;
		}
		
		ftp_close($ftp);
		
		return true;
	}
}
