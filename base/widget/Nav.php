<?php

abstract class Nav
{
	static function widget($params)
	{
		$_id = isset($params['id']) ? $params['id'] : 'menu';
		$_class = isset($params['class']) ? $params['class'] : 'navbar';
		$items = [];
		
		foreach ($params['items'] as $i)
		{
			$url = $i[0];
			$label = $i[1];
			$class = isset($i['class']) ? $i['class'] : '';
			$condition = isset($i['condition']) ? $i['condition'] : null;
			
			if ($condition || $condition === null)
			{
				$items[] = Html::a(Url::to($url), $label, $class ? ['class' => $class] : '', false);
			}
		}
		
		$buffer = "";
		
		foreach ($items as $i)
		{
			$buffer .= $i;
		}
		
		return Html::tag('div', $buffer, ['id' => $_id, 'class' => $_class], false);
	}
}