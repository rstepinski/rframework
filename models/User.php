<?php

class User extends ActiveRecord
{
	const SCENARIO_LOGIN = 'login';
	
	const ROLE_ADMIN = 1;
	const ROLE_GUEST = 0;
	
	function attributeLabels()
	{
		return [
			'username' => 'Login',
			'firstName' => 'Imię',
			'lastName' => 'Nazwisko',
			'password' => 'Hasło'
		];
	}
	
	function rules()
	{
		switch ($this->scenario)
		{
			case self::SCENARIO_LOGIN: return ['name' => 'required', 'password' => 'required'];
			default: return ['name' => 'required'];
		}
	}
	
	function login($password)
	{
		if (hash_equals($this->password, md5($password)))
		{
			App::login($this);
			return true;
		}
		return false;
	}
	
	function getFullName()
	{
		return $this->firstName.' '.$this->lastName;
	}
}

