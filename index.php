<?php			

define("HOST", $_SERVER['SERVER_NAME'].'/calendar');
define("ROOTPATH", dirname(__FILE__));
define("BASEDIR", substr(dirname(__FILE__), 15));
define("VIEWS", ROOTPATH.'/views/');
define("MODELS", ROOTPATH.'/models/');
define("CONTROLLERS", ROOTPATH.'/controllers/');
define("BASE", ROOTPATH.'/base/');

const DEBUG_PRODUCTION = 0;
const DEBUG_DEV = 1;
define("DEBUG_MODE", 1);

include(BASE."bootstrap.php");
include(BASE."errorhandler.php");

$reference = isset($_GET['r']) 
	? explode("/", $_GET['r']) 
	: [
		App::$config['app']['defaultController'], 
		App::$config['app']['defaultAction']
	];
$cname = ucfirst($reference[0]).'Controller';
$aname = isset($reference[1]) ? str_replace(';', '', $reference[1]) : App::$config['app']['defaultAction'];

session_start();

App::init();

call_user_func_array([new $cname, $aname], []);
