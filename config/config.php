<?php

return [
	'db' => [
		'host' => '',
		'user' => '',
		'pass' => '',
		'db' => '',
	],
	'ftp' => [
		'host' => '',
		'user' => '',
		'pass' => '',
		'basedir' => '',
	],
	'app' => [
		'defaultController' => 'site',
		'defaultAction' => 'index',
		'uploadPath' => 'files',
		'maxFileSize' => 1024 * 1024 * 10,
	],
];