<?php

class SiteController extends BaseController
{
	function permissions()
	{
		return [
			'index' => self::ACCESS_GUEST,
			'login' => self::ACCESS_GUEST,
		];
	}
	
	function actionIndex($date = null)
	{		
		$models = Post::find()->order('date DESC')->all();
		
		return $this->render('site/index', ['date' => $date, 'models' => $models]);
	}
	
	function actionLogout()
	{
		App::logout();
		App::addFlash('success', 'Zostałeś wylogowany');
		App::redirect(Url::to('site/login')); 
	}
	
	function actionLogin()
	{
		$model = new User(User::SCENARIO_LOGIN);
		
		if ($model->load(App::$post))
		{
			if ($model->validate())
			{
				$user = User::find()->where(['username' => $model->username])->one();
				if ($user !== null && $user->login($model->password))
				{
					App::addFlash('success', 'Zostałeś zalogowany');
					App::redirect(Url::to('site/index'));
				}
				else
				{
					App::addFlash('danger', 'Błędna nazwa użytkownika lub hasło');
				}
			}
			else
			{
				$model->flashErrors();
			}
		}
		
		return $this->render('site/login');
	}
}