<?php

$form = new Form(new User);

$title = 'Zaloguj się';
echo Html::header($title);

echo $form->begin([
		'class' => 'form-horizontal', 
		'layout' => [
			'label' => 'col-xs-3', 
			'field' => 'col-xs-9', 
			'offset' => 'col-xs-offset-3'
		]
	]);
echo $form->field('username')->text();
echo $form->field('password')->password();
echo $form->end();

?>
