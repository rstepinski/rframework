<html>
	
	<head>
		
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
		<link rel="stylesheet" href="<?= BASEDIR ?>/site/css/bootstrap/bootstrap.css" />
		<link rel="stylesheet" href="<?= BASEDIR ?>/site/css/bootstrap/bootstrap-theme.css" />
		<link rel="stylesheet" href="<?= BASEDIR ?>/site/css/bootstrap/font-awesome.css" />
		<link rel="stylesheet" href="<?= BASEDIR ?>/site/css/main.css" />
		
	</head>
	
	<body>
		
		<div class="container-fluid">
			
			<div class="content">
				
				<?= Nav::widget([
					'items' => [
						['site/index', 'Home'],
						['site/login', 'Zaloguj się', 'condition' => !App::loggedIn()],
						['site/logout', 'Wyloguj się', 'condition' => App::loggedIn()],
					],
				]) ?>
			
				<?php if (App::hasFlash()): ?>

				<div class="callouts">

					<?php 
					foreach (App::getAllFlashes() as $flash)
					{
						echo Callout::show($flash['type'], $flash['message']);
					}
					?>

				</div>

				<?php endif;?>
				
				<?= $content ?>
				
			</div>
			
		</div>
		
		<script type="text/javascript" src="<?= BASEDIR ?>/site/js/jquery.js"></script>
		<script type="text/javascript" src="<?= BASEDIR ?>/site/js/bootstrap.js"></script>
		<script type="text/javascript" src="<?= BASEDIR ?>/site/js/main.js"></script>
		<script type="text/javascript" src="<?= BASEDIR ?>/site/js/npm.js"></script>
	</body>
	
</html>